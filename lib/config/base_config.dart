class BaseConfig {
  static const baseUrl = "https://restaurant-api.dicoding.dev";
  static const restoList = "/list";
  static const restoDetail = "/detail";
  static const restoSearch = "/search";
  static const restoReview = "/review";

  static const imageUrl = "$baseUrl/images/large/";
}
