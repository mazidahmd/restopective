import 'package:android_alarm_manager/android_alarm_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:restopective/bloc_observer.dart';
import 'package:restopective/shared/notification_helper.dart';
import 'package:restopective/shared/routes.dart';
import 'package:restopective/shared/utils/background_service.dart';

import 'common/style.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final NotificationHelper _notificationHelper = NotificationHelper();
  Bloc.observer = Observer();
  var appDir = await getApplicationDocumentsDirectory();
  Hive.init(appDir.path);
  await _notificationHelper.initNotifications(flutterLocalNotificationsPlugin);
  final BackgroundService _service = BackgroundService();
  _service.initializeIsolate();
  AndroidAlarmManager.initialize();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Restopective',
      theme: Style.theme(),
      initialRoute: Routes.splashScreen,
      routes: Routes.routes,
    );
  }
}
