import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:restopective/common/app_text.dart';

class PrimaryButton extends ElevatedButton {
  final String label;
  final IconData? prefixIcon;
  final IconData? suffixIcon;
  final Color? color;
  final Color? outlineColor;
  final Color? labelColor;
  final double? width;
  final double? height;

  PrimaryButton(
      {Key? key,
      void Function()? onPressed,
      required this.label,
      this.prefixIcon,
      this.suffixIcon,
      this.color,
      this.outlineColor,
      this.labelColor,
      this.height,
      this.width})
      : super(
            key: key,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if (prefixIcon != null)
                  Icon(
                    prefixIcon,
                    size: 18,
                    color: labelColor,
                  ),
                if (prefixIcon != null) const SizedBox(width: 10),
                Text(label, style: AppText.regularNormalBold()),
                if (suffixIcon != null) const SizedBox(width: 10),
                if (suffixIcon != null)
                  Icon(
                    suffixIcon,
                    size: 18,
                    color: labelColor,
                  )
              ],
            ),
            onPressed: onPressed,
            style: ElevatedButton.styleFrom(
                minimumSize: Size(width ?? 100, height ?? 35),
                primary: color ?? Get.theme.colorScheme.primary,
                elevation: 0,
                shape: RoundedRectangleBorder(
                    side: BorderSide(
                        color: outlineColor ??
                            color ??
                            Get.theme.colorScheme.primary,
                        width: 1),
                    borderRadius: BorderRadius.circular(5))));
}
