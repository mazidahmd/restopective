import 'package:flutter/material.dart';
import 'package:restopective/common/app_color.dart';
import 'package:restopective/common/app_text.dart';

class NormalTextField extends StatelessWidget {
  final TextEditingController controller;
  final String hint;
  final double? width;
  final bool? isRequired;
  final bool? isObscured;
  final bool? isEnabled;
  final int? maxLength;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final String? initValue;
  final String? labelText;
  final Color? backgroundColor;
  final TextInputType? keyboardType;
  final String? Function(String?)? validator;
  final void Function(String?)? onChanged;
  final bool? autocorrect;
  final String? errorText;

  const NormalTextField(
      {Key? key,
      required this.controller,
      required this.hint,
      this.width,
      this.isRequired,
      this.isObscured,
      this.isEnabled,
      this.maxLength,
      this.labelText,
      this.prefixIcon,
      this.suffixIcon,
      this.initValue,
      this.validator,
      this.onChanged,
      this.keyboardType,
      this.autocorrect,
      this.backgroundColor,
      this.errorText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: TextFormField(
        validator: validator,
        onChanged: onChanged,
        controller: controller,
        initialValue: initValue,
        autocorrect: autocorrect ?? false,
        obscureText: isObscured ?? false,
        keyboardType: keyboardType,
        enabled: isEnabled ?? true,
        maxLength: maxLength ?? 50,
        style: AppText.regularNormalBold(),
        decoration: InputDecoration(
            counterText: '',
            labelText: labelText,
            contentPadding:
                const EdgeInsets.symmetric(horizontal: 20, vertical: 17),
            prefixIcon: prefixIcon,
            suffixIcon: suffixIcon,
            filled: true,
            errorText: errorText,
            errorMaxLines: 3,
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide.none,
            ),
            hintText: hint,
            hintStyle: AppText.regularNormalMedium(),
            fillColor: backgroundColor ?? AppColor.background,
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide.none,
            ),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide.none),
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide.none,
            ),
            border: InputBorder.none),
      ),
    );
  }
}
