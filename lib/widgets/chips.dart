import 'package:flutter/material.dart';
import 'package:restopective/common/app_color.dart';
import 'package:restopective/common/app_text.dart';

class Chips extends StatelessWidget {
  const Chips({Key? key, required this.label, this.isActive = false})
      : super(key: key);

  final String label;
  final bool isActive;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(right: 10),
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        decoration: BoxDecoration(
            color: isActive ? AppColor.primary : AppColor.surface,
            borderRadius: BorderRadius.circular(20)),
        child: Text(label, style: AppText.smallNormalBold()));
  }
}
