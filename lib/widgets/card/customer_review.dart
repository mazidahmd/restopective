import 'package:flutter/material.dart';
import 'package:restopective/common/app_color.dart';
import 'package:restopective/common/app_text.dart';
import 'package:restopective/data/model/customer_review.dart';

class ReviewCard extends StatelessWidget {
  const ReviewCard({
    Key? key,
    required this.customerReviews,
  }) : super(key: key);

  final CustomerReview? customerReviews;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.symmetric(vertical: 5),
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        decoration: BoxDecoration(color: AppColor.surface),
        child: Row(children: [
          const CircleAvatar(
              backgroundImage: NetworkImage(
                  "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png")),
          const SizedBox(width: 20),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(customerReviews?.name ?? "",
                        style: AppText.smallNormalBold()),
                    Text(customerReviews?.date ?? "",
                        style: AppText.tinyNormalRegular()),
                  ],
                ),
                const SizedBox(height: 5),
                Text(customerReviews?.review ?? "",
                    style: AppText.tinyTightRegular())
              ],
            ),
          )
        ]));
  }
}
