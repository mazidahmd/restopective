import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:restopective/common/app_color.dart';
import 'package:restopective/common/app_text.dart';
import 'package:restopective/data/model/restaurant.dart';
import 'package:restopective/shared/routes.dart';
import 'package:restopective/shared/utils/parse_util.dart';

class RestaurantCard extends StatelessWidget {
  const RestaurantCard({Key? key, required this.resto}) : super(key: key);

  final Restaurant resto;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.toNamed(Routes.detailPage, arguments: resto.id);
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
              color: Colors.black.withOpacity(0.05),
              blurRadius: 20,
              offset: const Offset(0, 5))
        ], color: AppColor.surface, borderRadius: BorderRadius.circular(20)),
        child: Row(
          children: [
            Hero(
              tag: ParseUtil().parseImageUrl(resto.pictureId) ??
                  "https://lh3.googleusercontent.com/proxy/Qf-CcQaTPPATbfB4_DsvFX-zR4C8beLH3Uu6bbDx51LyeIwh6YdSmbVb3KnjcZfSUpTEV81ng_F4wirOfDEUuLJtLkpPksmT6PheYEBuog",
              child: Container(
                width: 120,
                height: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(ParseUtil()
                                .parseImageUrl(resto.pictureId) ??
                            "https://lh3.googleusercontent.com/proxy/Qf-CcQaTPPATbfB4_DsvFX-zR4C8beLH3Uu6bbDx51LyeIwh6YdSmbVb3KnjcZfSUpTEV81ng_F4wirOfDEUuLJtLkpPksmT6PheYEBuog"))),
              ),
            ),
            const SizedBox(width: 15),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        resto.name ?? "-",
                        style: AppText.smallNormalBold(
                            color: AppColor.primaryVariant),
                      ),
                      Icon(Icons.keyboard_arrow_right_sharp,
                          color: AppColor.primaryVariant)
                    ],
                  ),
                  const SizedBox(height: 10),
                  Row(
                    children: [
                      Icon(
                        Icons.location_on,
                        size: 15,
                        color: AppColor.secondaryVariant,
                      ),
                      const SizedBox(width: 5),
                      Text(
                        resto.city ?? "-",
                        style: AppText.tinyNormalBold(
                            color: AppColor.secondaryVariant),
                      )
                    ],
                  ),
                  const SizedBox(height: 5),
                  Row(
                    children: [
                      Icon(
                        Icons.star,
                        size: 15,
                        color: AppColor.primary,
                      ),
                      const SizedBox(width: 5),
                      Text(
                        resto.rating.toString(),
                        style: AppText.tinyNormalBold(),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
