import 'package:flutter/material.dart';
import 'package:restopective/common/app_color.dart';
import 'package:shimmer/shimmer.dart';

class RestaurantShimmerCard extends StatelessWidget {
  const RestaurantShimmerCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: AppColor.secondaryVariant,
      highlightColor: AppColor.primary,
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
              color: Colors.black.withOpacity(0.05),
              blurRadius: 20,
              offset: const Offset(0, 5))
        ], color: AppColor.surface, borderRadius: BorderRadius.circular(20)),
        child: Row(
          children: [
            Container(
              width: 120,
              height: 100,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            const SizedBox(width: 15),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: const [
                      Text("This is resto name"),
                      Icon(Icons.keyboard_arrow_right_sharp)
                    ],
                  ),
                  const SizedBox(height: 10),
                  Row(
                    children: const [
                      Icon(Icons.location_on, size: 15),
                      SizedBox(width: 5),
                      Text("This is city"),
                    ],
                  ),
                  const SizedBox(height: 5),
                  Row(
                    children: const [
                      Icon(Icons.star, size: 15),
                      SizedBox(width: 5),
                      Text("Rating")
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
