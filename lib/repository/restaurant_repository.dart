import 'package:restopective/data/model/restaurant.dart';
import 'package:restopective/service/restaurant_service.dart';
import 'package:restopective/shared/database.dart';

class RestaurantRepository {
  final RestaurantService _restaurantService = RestaurantService();

  Future<List<Restaurant>?> getListRestaurant() async {
    var responseData = await _restaurantService.getListRestaurant();
    return responseData?.restaurants;
  }

  Future<List<Restaurant>?> getSearchRestaurant(String name) async {
    var responseData = await _restaurantService.getSearchRestaurant(name);
    return responseData?.restaurants;
  }

  Future<Restaurant?> getDetailRestaurant(String id) async {
    var responseData = await _restaurantService.getDetailRestaurant(id);
    return responseData?.restaurant;
  }

  Future sendReviewRestaurant(String id, String name, String review) async {
    var responseData =
        await _restaurantService.sendReviewRestaurant(id, name, review);
    return responseData?.restaurant;
  }

  Future addToFavourite(String id) async {
    var _data =
        await Database().getData(boxName: "restaurantBox", key: "favourites");
    var _listFav = [];

    if (_data != null) {
      _listFav = (_data as String).split(",").toList();
      _listFav.add(id);
    } else {
      _listFav = <String>[id];
    }
    await Database().putString(
        boxName: "restaurantBox", key: "favourites", value: _listFav.join(","));
  }

  Future removeFromFavourite(String id) async {
    var _data =
        await Database().getData(boxName: "restaurantBox", key: "favourites");

    if (_data != null) {
      var _listFav = (_data as String).split(",").toList();
      _listFav.removeWhere((restoId) => restoId == id);
      await Database().putString(
          boxName: "restaurantBox",
          key: "favourites",
          value: _listFav.join(","));
    }
  }

  Future<bool> isFavouriteResto(String id) async {
    var _data =
        await Database().getData(boxName: "restaurantBox", key: "favourites");

    if (_data != null) {
      var _listFav = (_data as String).split(",").toList();
      if (_listFav.contains(id)) return true;
    }
    return false;
  }

  Future<List<String>?> getListFavouriteId() async {
    var _data =
        await Database().getData(boxName: "restaurantBox", key: "favourites");
    var _listFav = <String>[];

    if (_data == null) return null;
    _listFav = (_data as String).split(",").toList();
    return _listFav;
  }
}
