import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:restopective/data/model/restaurants.dart';

class DummyData {
  static final DummyData _instance = DummyData._internal();
  DummyData._internal();

  Restaurants restaurants = Restaurants();

  factory DummyData() {
    return _instance;
  }

  Future<void> syncDummyData() async {
    String data = await rootBundle.loadString('assets/data/dummy.json');
    final jsonResult = jsonDecode(data);
    restaurants = Restaurants.fromJson(jsonResult);
  }
}
