import 'package:equatable/equatable.dart';
import 'package:restopective/data/model/food.dart';

class Menus extends Equatable {
  List<Food>? foods;
  List<Food>? drinks;

  Menus({this.foods, this.drinks});

  Menus.fromJson(Map<String, dynamic> json) {
    if (json['foods'] != null) {
      foods = <Food>[];
      json['foods'].forEach((v) {
        foods?.add(Food.fromJson(v));
      });
    }
    if (json['drinks'] != null) {
      drinks = <Food>[];
      json['drinks'].forEach((v) {
        drinks?.add(Food.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (foods != null) {
      data['foods'] = foods?.map((v) => v.toJson()).toList();
    }
    if (drinks != null) {
      data['drinks'] = drinks?.map((v) => v.toJson()).toList();
    }
    return data;
  }

  @override
  List<Object?> get props => [drinks, foods];
}
