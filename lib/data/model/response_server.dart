import 'package:restopective/data/model/restaurant.dart';

class ResponseServer {
  bool? error;
  String? message;
  int? founded;
  int? count;
  List<Restaurant>? restaurants;
  Restaurant? restaurant;

  ResponseServer({
    this.error,
    this.message,
    this.founded,
    this.count,
    this.restaurants,
    this.restaurant,
  });

  factory ResponseServer.fromJson(Map<String, dynamic> json) => ResponseServer(
        error: json["name"],
        message: json["message"],
        restaurant: json['restaurant'] != null
            ? Restaurant.fromJson(json['restaurant'])
            : null,
        founded: json["date"],
        count: json['count'],
        restaurants: (json['restaurants'] != null)
            ? List.from(json['restaurants'].map((x) => Restaurant.fromJson(x)))
            : null,
      );

  Map<String, dynamic> toJson() => {
        "error": error,
        "message": message,
        "founded": founded,
        "count": count,
        "restaurant": restaurant?.toJson(),
        "restaurants": restaurants?.map((v) => v.toJson()).toList()
      };
}
