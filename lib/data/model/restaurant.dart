import 'package:equatable/equatable.dart';
import 'package:restopective/data/model/customer_review.dart';
import 'package:restopective/data/model/menus.dart';

class Restaurant extends Equatable {
  String? id;
  String? name;
  String? description;
  String? pictureId;
  String? city;
  double? rating;
  List<String>? categories;
  Menus? menus;
  List<CustomerReview>? customerReviews;

  Restaurant(
      {this.id,
      this.name,
      this.description,
      this.pictureId,
      this.city,
      this.categories,
      this.rating,
      this.customerReviews,
      this.menus});

  Restaurant.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    pictureId = json['pictureId'];
    city = json['city'];
    rating = double.tryParse(json['rating'].toString());
    menus = json['menus'] != null ? Menus.fromJson(json['menus']) : null;
    if (json['categories'] != null) {
      categories = [];
      json['categories'].forEach((v) {
        categories?.add(v['name']);
      });
    }
    if (json['customerReviews'] != null) {
      customerReviews = <CustomerReview>[];
      json['customerReviews'].forEach((v) {
        customerReviews?.add(CustomerReview.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['description'] = description;
    data['pictureId'] = pictureId;
    data['city'] = city;
    data['rating'] = rating;
    if (menus != null) {
      data['menus'] = menus?.toJson();
    }
    data['categories'] = customerReviews != null
        ? List<dynamic>.from(categories!.map((x) => {"name": x}))
        : null;
    data['customerReviews'] = customerReviews != null
        ? List<dynamic>.from(customerReviews!.map((x) => x.toJson()))
        : null;
    return data;
  }

  @override
  List<Object?> get props => [
        id,
        name,
        description,
        pictureId,
        city,
        rating,
        menus,
        categories,
        customerReviews
      ];
}
