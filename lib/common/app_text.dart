import 'package:flutter/material.dart';

class AppText {
  static TextStyle title1Bold({Color? color}) => TextStyle(
      fontSize: 48, height: 1.25, color: color, fontWeight: FontWeight.w700);

  static TextStyle title2Bold({Color? color}) => TextStyle(
      fontSize: 32, height: 1.25, color: color, fontWeight: FontWeight.w700);

  static TextStyle title3Bold({Color? color}) => TextStyle(
      fontSize: 24, height: 1.25, color: color, fontWeight: FontWeight.w700);

  static TextStyle largeNoneBold({Color? color}) => TextStyle(
      fontSize: 18, height: 0, color: color, fontWeight: FontWeight.w700);

  static TextStyle largeNoneMedium({Color? color}) => TextStyle(
      fontSize: 18, height: 0, color: color, fontWeight: FontWeight.w500);

  static TextStyle largeNoneRegular({Color? color}) => TextStyle(
      fontSize: 18, height: 0, color: color, fontWeight: FontWeight.w400);

  static TextStyle largeTightBold({Color? color}) => TextStyle(
      fontSize: 18, height: 1.5, color: color, fontWeight: FontWeight.w700);

  static TextStyle largeTightMedium({Color? color}) => TextStyle(
      fontSize: 18, height: 1.5, color: color, fontWeight: FontWeight.w500);

  static TextStyle largeTightRegular({Color? color}) => TextStyle(
      fontSize: 18, height: 1.5, color: color, fontWeight: FontWeight.w400);

  static TextStyle largeNormalBold({Color? color}) => TextStyle(
      fontSize: 18, height: 1, color: color, fontWeight: FontWeight.w700);

  static TextStyle largeNormalMedium({Color? color}) => TextStyle(
      fontSize: 18, height: 1, color: color, fontWeight: FontWeight.w500);

  static TextStyle largeNormalRegular({Color? color}) => TextStyle(
      fontSize: 18, height: 1, color: color, fontWeight: FontWeight.w400);

  static TextStyle regularNoneBold({Color? color}) => TextStyle(
      fontSize: 16, height: 0, color: color, fontWeight: FontWeight.w700);

  static TextStyle regularNoneMedium({Color? color}) => TextStyle(
      fontSize: 16, height: 0, color: color, fontWeight: FontWeight.w500);

  static TextStyle regularNoneRegular({Color? color}) => TextStyle(
      fontSize: 16, height: 0, color: color, fontWeight: FontWeight.w400);

  static TextStyle regularTightBold({Color? color}) => TextStyle(
      fontSize: 16, height: 1.5, color: color, fontWeight: FontWeight.w700);

  static TextStyle regularTightMedium({Color? color}) => TextStyle(
      fontSize: 16, height: 1.5, color: color, fontWeight: FontWeight.w500);

  static TextStyle regularTightRegular({Color? color}) => TextStyle(
      fontSize: 16, height: 1.5, color: color, fontWeight: FontWeight.w400);

  static TextStyle regularNormalBold({Color? color}) => TextStyle(
      fontSize: 16, height: 1, color: color, fontWeight: FontWeight.w700);

  static TextStyle regularNormalMedium({Color? color}) => TextStyle(
      fontSize: 16, height: 1, color: color, fontWeight: FontWeight.w500);

  static TextStyle regularNormalRegular({Color? color}) => TextStyle(
      fontSize: 16, height: 1, color: color, fontWeight: FontWeight.w400);

  static TextStyle smallNoneMedium({Color? color}) => TextStyle(
      fontSize: 14, height: 0, color: color, fontWeight: FontWeight.w500);

  static TextStyle smallNoneRegular({Color? color}) => TextStyle(
      fontSize: 14, height: 0, color: color, fontWeight: FontWeight.w400);

  static TextStyle smallTightBold({Color? color}) => TextStyle(
      fontSize: 14, height: 1.5, color: color, fontWeight: FontWeight.w700);

  static TextStyle smallTightMedium({Color? color}) => TextStyle(
      fontSize: 14, height: 1.5, color: color, fontWeight: FontWeight.w500);

  static TextStyle smallTightRegular({Color? color}) => TextStyle(
      fontSize: 14, height: 1.5, color: color, fontWeight: FontWeight.w400);

  static TextStyle smallNormalBold({Color? color}) => TextStyle(
      fontSize: 14, height: 1, color: color, fontWeight: FontWeight.w700);

  static TextStyle smallNormalMedium({Color? color}) => TextStyle(
      fontSize: 14, height: 1, color: color, fontWeight: FontWeight.w500);

  static TextStyle smallNormalRegular({Color? color}) => TextStyle(
      fontSize: 14, height: 1, color: color, fontWeight: FontWeight.w400);

  static TextStyle tinyNonelBold({Color? color}) => TextStyle(
      fontSize: 12, height: 0, color: color, fontWeight: FontWeight.w400);

  static TextStyle tinyNoneMedium({Color? color}) => TextStyle(
      fontSize: 12, height: 0, color: color, fontWeight: FontWeight.w500);

  static TextStyle tinyNoneRegular({Color? color}) => TextStyle(
      fontSize: 12, height: 0, color: color, fontWeight: FontWeight.w400);

  static TextStyle tinyTightBold({Color? color}) => TextStyle(
      fontSize: 12, height: 1.5, color: color, fontWeight: FontWeight.w700);

  static TextStyle tinyTightMedium({Color? color}) => TextStyle(
      fontSize: 12, height: 1.5, color: color, fontWeight: FontWeight.w500);

  static TextStyle tinyTightRegular({Color? color}) => TextStyle(
      fontSize: 12, height: 1.5, color: color, fontWeight: FontWeight.w400);

  static TextStyle tinyNormalBold({Color? color}) => TextStyle(
      fontSize: 12, height: 1, color: color, fontWeight: FontWeight.w700);

  static TextStyle tinyNormalMedium({Color? color}) => TextStyle(
      fontSize: 12, height: 1, color: color, fontWeight: FontWeight.w500);

  static TextStyle tinyNormalRegular({Color? color}) => TextStyle(
      fontSize: 12, height: 1, color: color, fontWeight: FontWeight.w400);
}
