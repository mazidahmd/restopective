import 'package:flutter/material.dart';

class AppColor {
  static Color get primary => const Color(0xFFFFC536);
  static Color get primaryVariant => const Color(0xFFDB744D);
  static Color get onPrimary => const Color(0xFF110D16);
  static Color get secondary => const Color(0xFF2A212F);
  static Color get secondaryVariant => const Color(0xFFACA7AD);
  static Color get onSecondary => const Color(0xFFFFFFFF);
  static Color get background => const Color(0xFFFCFAF8);
  static Color get onBackground => const Color(0xFF110D16);
  static Color get surface => const Color(0xFFFFFFFF);
  static Color get onSurface => const Color(0xFF110D16);
  static Color get textColor => const Color(0xFF110D16);
  static Color get brightness => const Color(0xFFFCFAF8);
  static Color get error => const Color(0xFFFF5247);
  static Color get onError => const Color(0xFFFFFFFF);
}
