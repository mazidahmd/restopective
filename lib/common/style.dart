import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:restopective/common/app_color.dart';
import 'package:restopective/common/app_text.dart';

class Style {
  static ThemeData theme() => ThemeData(
      brightness: Brightness.light,
      backgroundColor: AppColor.background,
      appBarTheme: const AppBarTheme(color: Colors.transparent, elevation: 0),
      colorScheme: ColorScheme(
          primary: AppColor.primary,
          primaryVariant: AppColor.primaryVariant,
          secondary: AppColor.secondary,
          secondaryVariant: AppColor.secondaryVariant,
          surface: AppColor.surface,
          background: AppColor.background,
          onPrimary: AppColor.onPrimary,
          onSecondary: AppColor.secondary,
          onSurface: AppColor.onSurface,
          onBackground: AppColor.onBackground,
          error: AppColor.error,
          onError: AppColor.onError,
          brightness: Brightness.light),
      fontFamily: GoogleFonts.inter().fontFamily,
      textTheme: TextTheme(
          bodyText1: AppText.regularNormalRegular(),
          bodyText2: AppText.regularTightRegular(),
          caption: AppText.smallNormalRegular(),
          subtitle1: AppText.largeNormalBold(),
          subtitle2: AppText.largeTightBold(),
          headline1: AppText.title1Bold(),
          headline2: AppText.title2Bold(),
          headline3: AppText.title3Bold(),
          button: AppText.regularNoneBold(),
          overline: AppText.tinyNormalMedium()));
}
