import 'package:flutter/material.dart';

class AppImage {
  static AssetImage get background =>
      const AssetImage("assets/images/bg-pattern.jpg");
  static AssetImage get logoHorzontal =>
      const AssetImage("assets/images/logo/restopective-logo.png");
  static AssetImage get logoVertical =>
      const AssetImage("assets/images/logo/restopective.png");
}
