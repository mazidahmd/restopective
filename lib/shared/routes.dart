import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:restopective/ui/detail/add_favourite_cubit/add_favourite_cubit.dart';
import 'package:restopective/ui/detail/cubit/detail_cubit.dart';
import 'package:restopective/ui/detail/detail_page.dart';
import 'package:restopective/ui/detail/review_cubit/review_cubit.dart';
import 'package:restopective/ui/favourite/cubit/favourite_cubit.dart';
import 'package:restopective/ui/favourite/favourite_page.dart';
import 'package:restopective/ui/home/cubit/home_cubit.dart';
import 'package:restopective/ui/home/home_page.dart';
import 'package:restopective/ui/settings/cubit/schedule_cubit.dart';
import 'package:restopective/ui/settings/setting_page.dart';
import 'package:restopective/ui/splash_screen.dart';

class Routes {
  static String splashScreen = "/";
  static String homePage = "/home";
  static String detailPage = "/detail";
  static String favouritePage = "/favourite";
  static String settingPage = "/setting";

  static Map<String, WidgetBuilder> routes = {
    splashScreen: (context) => const SplashScreen(),
    homePage: (context) => BlocProvider(
          create: (context) => HomeCubit()..fetchListRestaurant(),
          child: const HomePage(),
        ),
    detailPage: (context) => MultiBlocProvider(providers: [
          BlocProvider(
            create: (context) => DetailCubit(),
          ),
          BlocProvider(
            create: (context) => ReviewCubit(),
          ),
          BlocProvider(
            create: (context) => AddFavouriteCubit(),
          )
        ], child: const DetailPage()),
    favouritePage: (context) => BlocProvider(
        create: (context) => FavouriteCubit()..getListFavouriteResto(),
        child: const FavouritePage()),
    settingPage: (context) => BlocProvider(
          create: (context) => ScheduleCubit()..getScheduleStatus(),
          child: const SettingPage(),
        )
  };
}
