import 'dart:ui';
import 'dart:isolate';

import 'package:restopective/data/model/restaurants.dart';
import 'package:restopective/main.dart';
import 'package:restopective/repository/restaurant_repository.dart';
import 'package:restopective/shared/notification_helper.dart';

final ReceivePort port = ReceivePort();

class BackgroundService {
  static final RestaurantRepository _restaurantRepository =
      RestaurantRepository();
  static BackgroundService? _instance;
  static const String _isolateName = 'isolate';
  static SendPort? _uiSendPort;

  BackgroundService._internal() {
    _instance = this;
  }

  factory BackgroundService() => _instance ?? BackgroundService._internal();

  void initializeIsolate() {
    IsolateNameServer.registerPortWithName(
      port.sendPort,
      _isolateName,
    );
  }

  static Future<void> callback() async {
    print('Alarm fired!');
    final NotificationHelper _notificationHelper = NotificationHelper();
    var result = await _restaurantRepository.getListRestaurant();
    await _notificationHelper.showNotification(
        flutterLocalNotificationsPlugin, Restaurants(restaurants: result));

    _uiSendPort ??= IsolateNameServer.lookupPortByName(_isolateName);
    _uiSendPort?.send(null);
  }
}
