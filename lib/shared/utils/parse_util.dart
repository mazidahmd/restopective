import 'package:restopective/config/base_config.dart';

class ParseUtil {
  String? parseImageUrl(String? pictureId) {
    if (pictureId == null) return null;
    return "${BaseConfig.imageUrl}$pictureId";
  }
}
