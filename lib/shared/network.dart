import 'package:dio/dio.dart';
import 'package:restopective/config/base_config.dart';

class Network {
  static Network? _instance;

  Network._internal() {
    _instance = this;
  }

  final Dio dio = Dio(BaseOptions(baseUrl: BaseConfig.baseUrl));

  factory Network() => _instance ?? Network._internal();
}
