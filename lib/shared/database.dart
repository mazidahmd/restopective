import 'dart:convert';

import 'package:hive/hive.dart';

class Database {
  Database();

  Future<void> putString(
      {required String boxName,
      required String key,
      required String value}) async {
    Box box = await Hive.openBox(boxName);
    await box.put(key, value);
    await box.close();
    return;
  }

  Future<void> deleteData({required String name}) async {
    Box box = await Hive.openBox(name);
    await box.deleteFromDisk();
    await box.close();
    return;
  }

  Future<void> putData(
      {required String boxName,
      required String key,
      required Map<String, dynamic> data}) async {
    Box box = await Hive.openBox(boxName);
    final String stringJson = json.encode(data);
    await box.put(key, stringJson);
    await box.close();
    return;
  }

  Future<void> updateBox(
      {required String boxName,
      required Map<String, dynamic> value,
      required int index}) async {
    Box box = await Hive.openBox(boxName);
    await box.putAt(index, value);
    await box.close();
    return;
  }

  Future<dynamic> getData(
      {required String boxName, required String key}) async {
    Box box = await Hive.openBox(boxName);
    final value = await box.get(key);
    await box.close();
    return value;
  }

  Future<dynamic> getList(
      {required String boxName, required String key}) async {
    Box box = await Hive.openBox(boxName);
    dynamic value = await box.get(key);
    await box.close();
    return value;
  }
}
