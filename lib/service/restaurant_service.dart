import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:restopective/config/base_config.dart';
import 'package:restopective/data/model/response_server.dart';
import 'package:restopective/shared/network.dart';
import 'package:restopective/shared/utils/exception_util.dart';

class RestaurantService {
  final Dio _dio = Network().dio;

  Future<ResponseServer?> getListRestaurant() async {
    try {
      Response response = await _dio.get(BaseConfig.restoList);
      if (response.statusCode! >= 200 && response.statusCode! <= 300) {
        return ResponseServer.fromJson(response.data);
      }
      throw response.data['message'];
    } on DioError catch (e) {
      ExceptionUtil().parseException(e);
      throw e.toString();
    } catch (e) {
      throw e.toString();
    }
  }

  Future<ResponseServer?> getSearchRestaurant(String query) async {
    try {
      Response response = await _dio.get("${BaseConfig.restoSearch}?q=$query");
      if (response.statusCode! >= 200 && response.statusCode! <= 300) {
        return ResponseServer.fromJson(response.data);
      }
      throw response.data['message'];
    } on DioError catch (e) {
      ExceptionUtil().parseException(e);
      throw e.toString();
    } catch (e) {
      throw e.toString();
    }
  }

  Future<ResponseServer?> getDetailRestaurant(String id) async {
    try {
      Response response = await _dio.get("${BaseConfig.restoDetail}/$id");
      if (response.statusCode! >= 200 && response.statusCode! <= 300) {
        return ResponseServer.fromJson(response.data);
      }
      throw response.data['message'];
    } on DioError catch (e) {
      ExceptionUtil().parseException(e);
      throw e.toString();
    } catch (e) {
      throw e.toString();
    }
  }

  Future<ResponseServer?> sendReviewRestaurant(
      String id, String name, String review) async {
    try {
      var data = {"id": id, "name": name, "review": review};

      Response response =
          await _dio.post(BaseConfig.restoReview, data: jsonEncode(data));

      if (response.statusCode! >= 200 && response.statusCode! <= 300) {
        return ResponseServer.fromJson(response.data);
      }
      throw response.data['message'];
    } on DioError catch (e) {
      ExceptionUtil().parseException(e);
      throw e.toString();
    } catch (e) {
      throw e.toString();
    }
  }
}
