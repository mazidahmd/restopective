part of 'home_cubit.dart';

@immutable
abstract class HomeState {}

class HomeInitial extends HomeState {}

class HomeLoading extends HomeState {}

class HomeEmpty extends HomeState {}

class HomeLoaded extends HomeState {
  final List<Restaurant> restaurants;

  HomeLoaded({required this.restaurants});
}

class HomeFailed extends HomeState {
  final String message;

  HomeFailed(this.message);
}
