import 'package:android_alarm_manager/android_alarm_manager.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:restopective/data/model/restaurant.dart';
import 'package:restopective/repository/restaurant_repository.dart';
import 'package:restopective/shared/date_time_helper.dart';
import 'package:restopective/shared/utils/background_service.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  final RestaurantRepository _restaurantRepository = RestaurantRepository();
  HomeCubit() : super(HomeInitial());

  void fetchListRestaurant() async {
    emit(HomeLoading());
    try {
      var data = await _restaurantRepository.getListRestaurant();
      if (data?.isNotEmpty ?? false) {
        emit(HomeLoaded(restaurants: data!));
      } else {
        emit(HomeEmpty());
      }
    } catch (e) {
      emit(HomeFailed(e.toString()));
    }
  }

  void searchRestaurant(String name) async {
    emit(HomeLoading());
    try {
      var data = await _restaurantRepository.getSearchRestaurant(name);
      if (data?.isNotEmpty ?? false) {
        emit(HomeLoaded(restaurants: data!));
      } else {
        emit(HomeEmpty());
      }
    } catch (e) {
      emit(HomeFailed(e.toString()));
    }
  }
}
