import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:restopective/common/app_color.dart';
import 'package:restopective/common/app_image.dart';
import 'package:restopective/common/app_text.dart';
import 'package:restopective/shared/notification_helper.dart';
import 'package:restopective/shared/routes.dart';
import 'package:restopective/ui/home/cubit/home_cubit.dart';
import 'package:restopective/widgets/card/restaurant_card.dart';
import 'package:restopective/widgets/card/restaurant_shimmer_card.dart';
import 'package:restopective/widgets/chips.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final NotificationHelper _notificationHelper = NotificationHelper();
  late HomeCubit _homeCubit;

  @override
  void initState() {
    super.initState();
    _notificationHelper.configureSelectNotificationSubject(Routes.detailPage);
    _homeCubit = BlocProvider.of<HomeCubit>(context);
  }

  void searchFunction(String value) {
    if (value.isEmpty) {
      _homeCubit.fetchListRestaurant();
    } else {
      _homeCubit.searchRestaurant(value);
    }
  }

  @override
  void dispose() {
    _homeCubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
          centerTitle: true,
          title: Image(
            image: AppImage.logoHorzontal,
            height: 30,
          ),
          actions: [
            IconButton(
                onPressed: () {
                  Get.toNamed(Routes.favouritePage);
                },
                icon: const Icon(Icons.favorite)),
            IconButton(
                onPressed: () {
                  Get.toNamed(Routes.settingPage);
                },
                icon: const Icon(Icons.settings))
          ]),
      body: NestedScrollView(
        headerSliverBuilder: (context, isScrolled) {
          return [
            SliverAppBar(
              collapsedHeight: 0,
              toolbarHeight: 0,
              backgroundColor: AppColor.surface,
              pinned: true,
              expandedHeight: 260,
              flexibleSpace: FlexibleSpaceBar(
                background: Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AppImage.background,
                            repeat: ImageRepeat.repeat,
                            colorFilter: ColorFilter.mode(
                                AppColor.primary.withOpacity(0.06),
                                BlendMode.dstATop),
                            scale: 0.01)),
                    child: Stack(
                      children: [
                        Positioned(
                          bottom: 0,
                          child: Container(
                            height: 200,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                              colors: [
                                AppColor.background,
                                AppColor.background.withOpacity(0),
                              ],
                            )),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(height: 100),
                              Text(
                                "Find good\nResto arround you",
                                style: AppText.title2Bold(),
                              ),
                              const SizedBox(height: 20),
                              Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 0),
                                decoration: BoxDecoration(
                                    color: AppColor.surface,
                                    borderRadius: BorderRadius.circular(30)),
                                child: TextFormField(
                                  onChanged: searchFunction,
                                  style: AppText.regularNormalRegular(),
                                  decoration: InputDecoration(
                                      prefixIcon: const Icon(Icons.search),
                                      prefixIconConstraints:
                                          const BoxConstraints(
                                              minWidth: 40, minHeight: 20),
                                      hintText: "Search your fav resto",
                                      border: InputBorder.none,
                                      hintStyle: AppText.smallNormalBold()),
                                ),
                              ),
                              const SizedBox(height: 20),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    "Find",
                                    style: AppText.title3Bold(),
                                  ),
                                  const SizedBox(width: 10),
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Text(
                                        "5km",
                                        style: AppText.largeTightBold(
                                            color: AppColor.primaryVariant),
                                      ),
                                      Icon(
                                        Icons.chevron_right_rounded,
                                        color: AppColor.primaryVariant,
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    )),
              ),
            ),
          ];
        },
        body: BlocConsumer<HomeCubit, HomeState>(
          listener: (context, state) {
            if (state is HomeFailed) {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text(state.message), backgroundColor: Colors.red));
            }
          },
          builder: (context, state) {
            if (state is HomeLoading) {
              return ListView.builder(
                  padding: EdgeInsets.zero,
                  shrinkWrap: true,
                  itemCount: 4,
                  physics: const NeverScrollableScrollPhysics(),
                  itemBuilder: (context, idx) {
                    return const RestaurantShimmerCard();
                  });
            }
            if (state is HomeEmpty) {
              return Center(
                child: Text(
                  "Sorry, there is no data here",
                  style: AppText.regularNoneBold(),
                ),
              );
            }
            if (state is HomeLoaded) {
              return SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                children: const [
                                  Chips(label: "Nearby", isActive: true),
                                  Chips(label: "Discount"),
                                  Chips(label: "Popular")
                                ],
                              )),
                          state.restaurants.isNotEmpty
                              ? ListView.builder(
                                  padding: EdgeInsets.zero,
                                  shrinkWrap: true,
                                  itemCount: state.restaurants.length,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemBuilder: (context, idx) {
                                    var _resto = state.restaurants[idx];
                                    return RestaurantCard(resto: _resto);
                                  })
                              : Container(
                                  padding: const EdgeInsets.all(100),
                                  child: Center(
                                    child: Text("Data not found",
                                        style: AppText.smallNormalBold()),
                                  ),
                                ),
                        ],
                      ),
                    )
                  ],
                ),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}
