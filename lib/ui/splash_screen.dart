import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:restopective/common/app_color.dart';
import 'package:restopective/common/app_image.dart';
import 'package:restopective/shared/routes.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  late Timer timer;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Timer(const Duration(seconds: 3), () {
      Get.offAllNamed(Routes.homePage);
    });

    var screenSize = MediaQuery.of(context).size;
    return Container(
      width: screenSize.width,
      height: screenSize.height,
      decoration: BoxDecoration(
          color: AppColor.surface,
          image: DecorationImage(
              image: AppImage.background,
              repeat: ImageRepeat.repeat,
              colorFilter: ColorFilter.mode(
                  AppColor.primary.withOpacity(0.06), BlendMode.dstATop),
              scale: 0.01)),
      child: Center(
          child: Image(
        image: AppImage.logoVertical,
        width: 250,
      )),
    );
  }
}
