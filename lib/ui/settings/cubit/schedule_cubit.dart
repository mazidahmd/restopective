import 'package:android_alarm_manager/android_alarm_manager.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:restopective/shared/date_time_helper.dart';
import 'package:restopective/shared/shared_preference_helper.dart';
import 'package:restopective/shared/utils/background_service.dart';

part 'schedule_state.dart';

class ScheduleCubit extends Cubit<ScheduleState> {
  ScheduleCubit() : super(ScheduleInitial());

  void scheduledNews(bool value) async {
    bool _isScheduled = value;
    emit(ScheduleLoading());
    try {
      if (_isScheduled) {
        print('Scheduling News Activated');
        await SharedPreferencesHelper()
            .setBoolValuesSF("isScheduled", _isScheduled);
        emit(ScheduleLoaded(await AndroidAlarmManager.periodic(
          const Duration(hours: 24),
          1,
          BackgroundService.callback,
          startAt: DateTimeHelper.format(),
          exact: true,
          wakeup: true,
        )));
      } else {
        print('Scheduling News Canceled');
        await SharedPreferencesHelper()
            .setBoolValuesSF("isScheduled", _isScheduled);
        emit(ScheduleLoaded(!(await AndroidAlarmManager.cancel(1))));
      }
    } catch (e) {
      emit(ScheduleFailed(e.toString()));
    }
  }

  void getScheduleStatus() async {
    emit(ScheduleLoading());
    try {
      bool? isScheduled =
          await SharedPreferencesHelper().getBoolValuesSF("isScheduled");
      emit(ScheduleLoaded(isScheduled ?? false));
    } catch (e) {
      emit(ScheduleFailed(e.toString()));
    }
  }
}
