part of 'schedule_cubit.dart';

@immutable
abstract class ScheduleState {}

class ScheduleInitial extends ScheduleState {}

class ScheduleLoading extends ScheduleState {}

class ScheduleLoaded extends ScheduleState {
  final bool isSchedule;

  ScheduleLoaded(this.isSchedule);
}

class ScheduleFailed extends ScheduleState {
  final String message;

  ScheduleFailed(this.message);
}
