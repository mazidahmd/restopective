import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:restopective/common/app_text.dart';
import 'package:restopective/ui/settings/cubit/schedule_cubit.dart';
import 'package:restopective/widgets/custom_dialog.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  late ScheduleCubit _scheduleCubit;

  @override
  void initState() {
    _scheduleCubit = BlocProvider.of<ScheduleCubit>(context);
    super.initState();
  }

  @override
  void dispose() {
    _scheduleCubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Setting",
          style: AppText.largeNormalMedium(),
        ),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Scheduling News',
                  style: AppText.regularNoneRegular(),
                ),
                BlocBuilder<ScheduleCubit, ScheduleState>(
                  builder: (context, state) {
                    if (state is ScheduleLoaded) {
                      return Switch.adaptive(
                        value: state.isSchedule,
                        onChanged: (value) async {
                          if (Platform.isIOS) {
                            CustomDialog().customDialog(context);
                          } else {
                            _scheduleCubit.scheduledNews(value);
                          }
                        },
                      );
                    }
                    return Container();
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
