import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:restopective/repository/restaurant_repository.dart';

part 'review_state.dart';

class ReviewCubit extends Cubit<ReviewState> {
  final RestaurantRepository _restaurantRepository = RestaurantRepository();
  ReviewCubit() : super(ReviewInitial());

  void sendReviewRestaurant(String id, String name, String review) async {
    emit(ReviewLoading());
    try {
      await _restaurantRepository.sendReviewRestaurant(id, name, review);
      emit(ReviewSuccess());
    } catch (e) {
      emit(ReviewFailed(e.toString()));
    }
  }
}
