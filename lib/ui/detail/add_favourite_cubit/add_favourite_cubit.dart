import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:restopective/repository/restaurant_repository.dart';

part 'add_favourite_state.dart';

class AddFavouriteCubit extends Cubit<AddFavouriteState> {
  final RestaurantRepository _restaurantRepository = RestaurantRepository();
  AddFavouriteCubit() : super(AddFavouriteInitial());

  void addToFavourite(String id, bool status) async {
    emit(AddFavouriteLoading());
    try {
      if (status) {
        await _restaurantRepository.removeFromFavourite(id);
        emit(AddFavouriteLoaded(isFavourite: false));
      } else {
        await _restaurantRepository.addToFavourite(id);
        emit(AddFavouriteLoaded(isFavourite: true));
      }
    } catch (e) {
      emit(AddFavouriteFailed(e.toString()));
    }
  }

  void checkFavourite(String id) async {
    emit(AddFavouriteLoading());
    try {
      var isFav = await _restaurantRepository.isFavouriteResto(id);
      emit(AddFavouriteLoaded(isFavourite: isFav));
    } catch (e) {
      emit(AddFavouriteFailed(e.toString()));
    }
  }
}
