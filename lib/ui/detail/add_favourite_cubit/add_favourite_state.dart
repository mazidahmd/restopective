part of 'add_favourite_cubit.dart';

@immutable
abstract class AddFavouriteState {}

class AddFavouriteInitial extends AddFavouriteState {}

class AddFavouriteLoading extends AddFavouriteState {}

class AddFavouriteLoaded extends AddFavouriteState {
  final bool isFavourite;
  AddFavouriteLoaded({required this.isFavourite});
}

class AddFavouriteFailed extends AddFavouriteState {
  final String message;

  AddFavouriteFailed(this.message);
}
