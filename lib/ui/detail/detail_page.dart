import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:restopective/common/app_color.dart';
import 'package:restopective/common/app_image.dart';
import 'package:restopective/common/app_text.dart';
import 'package:restopective/shared/utils/parse_util.dart';
import 'package:restopective/ui/detail/add_favourite_cubit/add_favourite_cubit.dart';
import 'package:restopective/ui/detail/cubit/detail_cubit.dart';
import 'package:restopective/ui/detail/review_cubit/review_cubit.dart';
import 'package:restopective/widgets/button/primary_button.dart';
import 'package:restopective/widgets/card/customer_review.dart';
import 'package:restopective/widgets/text_field/normal_text_field.dart';

class DetailPage extends StatefulWidget {
  const DetailPage({Key? key}) : super(key: key);

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String id = "";
  late DetailCubit cubit;
  late AddFavouriteCubit favCubit;
  late ReviewCubit reviewCubit;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _reviewController = TextEditingController();

  @override
  void initState() {
    cubit = BlocProvider.of<DetailCubit>(context);
    favCubit = BlocProvider.of<AddFavouriteCubit>(context);
    reviewCubit = BlocProvider.of<ReviewCubit>(context);
    cubit.fetchRestaurant(Get.arguments);
    favCubit.checkFavourite(Get.arguments);
    super.initState();
  }

  @override
  void dispose() {
    cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
      key: _scaffoldKey,
      extendBodyBehindAppBar: true,
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          _scaffoldKey.currentState!
              .showBottomSheet((context) => ReviewBottomsheet(
                    id: id,
                    nameController: _nameController,
                    reviewController: _reviewController,
                    cubit: reviewCubit,
                  ));
          _reviewController.clear();
          _nameController.clear();
        },
        backgroundColor: AppColor.primary,
        child: const Icon(Icons.reviews),
      ),
      appBar: AppBar(
          leading: Container(
        margin: const EdgeInsets.all(10),
        width: 20,
        height: 20,
        decoration:
            BoxDecoration(color: AppColor.surface, shape: BoxShape.circle),
        child: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(Icons.arrow_back, size: 20)),
      )),
      body: BlocListener<ReviewCubit, ReviewState>(
        listener: (context, state) {
          if (state is ReviewSuccess) {
            cubit.fetchRestaurant(id);
          }
        },
        child: BlocConsumer<DetailCubit, DetailState>(
          listener: (context, state) {
            if (state is DetailLoaded) {
              setState(() {
                id = state.restaurant.id!;
              });
            }
            if (state is DetailFailed) {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text(state.message), backgroundColor: Colors.red));
            }
          },
          builder: (context, state) {
            if (state is DetailLoading) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is DetailLoaded) {
              var resto = state.restaurant;
              return SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Stack(
                      alignment: Alignment.bottomCenter,
                      children: [
                        Container(
                            height: 400,
                            decoration: BoxDecoration(
                                color: AppColor.primary,
                                image: DecorationImage(
                                    image: AppImage.background,
                                    repeat: ImageRepeat.repeat,
                                    colorFilter: ColorFilter.mode(
                                        AppColor.primary.withOpacity(0.06),
                                        BlendMode.dstATop),
                                    scale: 0.01))),
                        Stack(
                          children: [
                            Container(
                              height: 200,
                              padding: const EdgeInsets.only(top: 130),
                              margin: const EdgeInsets.only(top: 100),
                              width: screenSize.width,
                              decoration: BoxDecoration(
                                  color: AppColor.background,
                                  borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(50),
                                      topRight: Radius.circular(50))),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        resto.name ?? "-",
                                        style: AppText.largeNormalBold(),
                                      ),
                                      const SizedBox(height: 15),
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Row(
                                              children: [
                                                Icon(
                                                  Icons.location_on_sharp,
                                                  color: AppColor.error,
                                                  size: 18,
                                                ),
                                                const SizedBox(width: 5),
                                                Text(
                                                  resto.city ?? "",
                                                  style:
                                                      AppText.smallTightBold(),
                                                )
                                              ],
                                            ),
                                            const SizedBox(width: 10),
                                            Row(
                                              children: [
                                                Icon(
                                                  Icons.star,
                                                  color: AppColor.primary,
                                                  size: 18,
                                                ),
                                                const SizedBox(width: 5),
                                                Text(
                                                  resto.rating.toString(),
                                                  style:
                                                      AppText.smallTightBold(),
                                                )
                                              ],
                                            ),
                                          ]),
                                    ],
                                  ),
                                  const SizedBox(width: 20),
                                  BlocBuilder<AddFavouriteCubit,
                                      AddFavouriteState>(
                                    builder: (context, state) {
                                      if (state is AddFavouriteLoaded) {
                                        return IconButton(
                                            onPressed: () {
                                              favCubit.addToFavourite(
                                                  Get.arguments,
                                                  state.isFavourite);
                                            },
                                            icon: state.isFavourite
                                                ? const Icon(Icons.favorite)
                                                : const Icon(
                                                    Icons.favorite_border));
                                      }
                                      return const CircularProgressIndicator();
                                    },
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin:
                                  const EdgeInsets.symmetric(horizontal: 30),
                              height: 200,
                              width: screenSize.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(30),
                                child: Hero(
                                  tag: ParseUtil()
                                          .parseImageUrl(resto.pictureId) ??
                                      "https://lh3.googleusercontent.com/proxy/Qf-CcQaTPPATbfB4_DsvFX-zR4C8beLH3Uu6bbDx51LyeIwh6YdSmbVb3KnjcZfSUpTEV81ng_F4wirOfDEUuLJtLkpPksmT6PheYEBuog",
                                  child: Image.network(
                                    ParseUtil()
                                            .parseImageUrl(resto.pictureId) ??
                                        "https://lh3.googleusercontent.com/proxy/Qf-CcQaTPPATbfB4_DsvFX-zR4C8beLH3Uu6bbDx51LyeIwh6YdSmbVb3KnjcZfSUpTEV81ng_F4wirOfDEUuLJtLkpPksmT6PheYEBuog",
                                    height: 200.0,
                                    width: screenSize.width,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 25),
                      child: Column(
                        children: [
                          Chip(
                              backgroundColor: AppColor.primary,
                              label: Text(
                                "Categories",
                                style: AppText.tinyNormalBold(),
                              )),
                          Wrap(
                              spacing: 10,
                              children: List.generate(
                                  resto.categories?.length ?? 0,
                                  (idx) => Chip(
                                      label: Text(resto.categories?[idx] ?? "",
                                          style: AppText.tinyNormalBold())))),
                          const SizedBox(height: 10),
                          Chip(
                              backgroundColor: AppColor.primary,
                              label: Text(
                                "Description",
                                style: AppText.tinyNormalBold(),
                              )),
                          Text(
                            resto.description ?? "-",
                            style: AppText.tinyTightMedium(),
                            textAlign: TextAlign.justify,
                          ),
                          const SizedBox(height: 10),
                          Chip(
                              backgroundColor: AppColor.primary,
                              label: Text(
                                "Menu",
                                style: AppText.tinyNormalBold(),
                              )),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Foods",
                                style: AppText.smallTightBold(),
                              ),
                              const Divider(),
                              Wrap(
                                  spacing: 10,
                                  children: List.generate(
                                      resto.menus?.foods?.length ?? 0,
                                      (idx) => Chip(
                                          label: Text(
                                              resto.menus?.foods?[idx].name ??
                                                  "",
                                              style:
                                                  AppText.tinyNormalBold()))))
                            ],
                          ),
                          const SizedBox(height: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Drinks",
                                style: AppText.smallTightBold(),
                              ),
                              const Divider(),
                              Wrap(
                                  spacing: 10,
                                  children: List.generate(
                                      resto.menus?.drinks?.length ?? 0,
                                      (idx) => Chip(
                                          label: Text(
                                              resto.menus?.drinks?[idx].name ??
                                                  "",
                                              style:
                                                  AppText.tinyNormalBold())))),
                            ],
                          ),
                          const SizedBox(height: 10),
                          Chip(
                              backgroundColor: AppColor.primary,
                              label: Text(
                                "Reviews",
                                style: AppText.tinyNormalBold(),
                              )),
                          const SizedBox(height: 10),
                          ListView.builder(
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              padding: EdgeInsets.zero,
                              itemCount: resto.customerReviews?.length ?? 0,
                              itemBuilder: (context, idx) {
                                return ReviewCard(
                                    customerReviews:
                                        resto.customerReviews?[idx]);
                              }),
                          const SizedBox(height: 100),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}

class ReviewBottomsheet extends StatefulWidget {
  const ReviewBottomsheet(
      {Key? key,
      required this.id,
      required this.nameController,
      required this.reviewController,
      required this.cubit})
      : super(key: key);
  final String id;
  final TextEditingController nameController;
  final TextEditingController reviewController;
  final ReviewCubit cubit;

  @override
  State<ReviewBottomsheet> createState() => _ReviewBottomsheetState();
}

class _ReviewBottomsheetState extends State<ReviewBottomsheet> {
  @override
  Widget build(BuildContext context) {
    return BlocListener<ReviewCubit, ReviewState>(
      listener: (context, state) {
        if (state is ReviewSuccess) {
          Get.back(result: true);
        }
      },
      child: Container(
        height: 300,
        padding: const EdgeInsets.all(20),
        decoration: BoxDecoration(
            color: AppColor.surface,
            borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(20), topRight: Radius.circular(20))),
        child: Column(
          children: [
            Text("Add Review", style: AppText.regularTightBold()),
            const SizedBox(height: 10),
            NormalTextField(
                controller: widget.nameController, hint: "Reviewer Name"),
            NormalTextField(
                controller: widget.reviewController, hint: "Review"),
            BlocBuilder<ReviewCubit, ReviewState>(
              builder: (context, state) {
                if (state is ReviewLoading) {
                  return const Center(child: CircularProgressIndicator());
                }
                return PrimaryButton(
                  label: "Send Review",
                  onPressed: () {
                    widget.cubit.sendReviewRestaurant(
                        widget.id,
                        widget.nameController.text,
                        widget.reviewController.text);
                  },
                  height: 40,
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
