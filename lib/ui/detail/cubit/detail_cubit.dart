import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:restopective/data/model/restaurant.dart';
import 'package:restopective/repository/restaurant_repository.dart';

part 'detail_state.dart';

class DetailCubit extends Cubit<DetailState> {
  final RestaurantRepository _restaurantRepository = RestaurantRepository();
  DetailCubit() : super(DetailInitial());

  void fetchRestaurant(String id) async {
    emit(DetailLoading());
    try {
      var data = await _restaurantRepository.getDetailRestaurant(id);
      if (data != null) {
        emit(DetailLoaded(restaurant: data));
      } else {
        emit(DetailEmpty());
      }
    } catch (e) {
      emit(DetailFailed(e.toString()));
    }
  }
}
