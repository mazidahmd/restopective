part of 'detail_cubit.dart';

@immutable
abstract class DetailState {}

class DetailInitial extends DetailState {}

class DetailLoading extends DetailState {}

class DetailEmpty extends DetailState {}

class DetailLoaded extends DetailState {
  final Restaurant restaurant;

  DetailLoaded({required this.restaurant});
}

class DetailFailed extends DetailState {
  final String message;

  DetailFailed(this.message);
}
