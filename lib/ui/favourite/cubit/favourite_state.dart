part of 'favourite_cubit.dart';

@immutable
abstract class FavouriteState {}

class FavouriteInitial extends FavouriteState {}

class FavouriteLoading extends FavouriteState {}

class FavouriteLoaded extends FavouriteState {
  final List<Restaurant> restaurants;

  FavouriteLoaded({required this.restaurants});
}

class FavouriteEmpty extends FavouriteState {}

class FavouriteFailed extends FavouriteState {
  final String message;

  FavouriteFailed(this.message);
}
