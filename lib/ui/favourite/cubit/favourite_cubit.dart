import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:restopective/data/model/restaurant.dart';
import 'package:restopective/repository/restaurant_repository.dart';

part 'favourite_state.dart';

class FavouriteCubit extends Cubit<FavouriteState> {
  final RestaurantRepository _restaurantRepository = RestaurantRepository();
  FavouriteCubit() : super(FavouriteInitial());

  void getListFavouriteResto() async {
    emit(FavouriteLoading());
    try {
      var _favRestos = await _restaurantRepository.getListFavouriteId();

      if (_favRestos != null) {
        if (_favRestos.isEmpty) {
          emit(FavouriteEmpty());
        } else {
          var _dataRestos = await _restaurantRepository.getListRestaurant();
          var _favRestosData = _dataRestos
              ?.where((data) => _favRestos.contains(data.id))
              .toList();
          if (_favRestosData?.isNotEmpty ?? false) {
            emit(FavouriteLoaded(restaurants: _favRestosData!));
          } else {
            emit(FavouriteEmpty());
          }
        }
      }
    } catch (e) {
      emit(FavouriteFailed(e.toString()));
    }
  }
}
