import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:restopective/common/app_text.dart';
import 'package:restopective/ui/favourite/cubit/favourite_cubit.dart';
import 'package:restopective/widgets/card/restaurant_card.dart';
import 'package:restopective/widgets/card/restaurant_shimmer_card.dart';

class FavouritePage extends StatefulWidget {
  const FavouritePage({Key? key}) : super(key: key);

  @override
  _FavouritePageState createState() => _FavouritePageState();
}

class _FavouritePageState extends State<FavouritePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Favourite List",
          style: AppText.largeNormalMedium(),
        ),
      ),
      body: BlocConsumer<FavouriteCubit, FavouriteState>(
          builder: (context, state) {
            if (state is FavouriteLoading) {
              return ListView.builder(
                  padding: EdgeInsets.zero,
                  shrinkWrap: true,
                  itemCount: 5,
                  physics: const NeverScrollableScrollPhysics(),
                  itemBuilder: (context, idx) {
                    return const RestaurantShimmerCard();
                  });
            }
            if (state is FavouriteEmpty) {
              return Center(
                child: Text(
                  "Favourite Empty",
                  style: AppText.regularNoneBold(),
                ),
              );
            }
            if (state is FavouriteLoaded) {
              return SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          state.restaurants.isNotEmpty
                              ? ListView.builder(
                                  padding: EdgeInsets.zero,
                                  shrinkWrap: true,
                                  itemCount: state.restaurants.length,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemBuilder: (context, idx) {
                                    var _resto = state.restaurants[idx];
                                    return RestaurantCard(resto: _resto);
                                  })
                              : Container(
                                  padding: const EdgeInsets.all(100),
                                  child: Center(
                                    child: Text("Data not found",
                                        style: AppText.smallNormalBold()),
                                  ),
                                ),
                        ],
                      ),
                    )
                  ],
                ),
              );
            }
            return Container();
          },
          listener: (context, state) {}),
    );
  }
}
