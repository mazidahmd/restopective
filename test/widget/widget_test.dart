import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:network_image_mock/network_image_mock.dart';
import 'package:restopective/data/model/customer_review.dart';
import 'package:restopective/data/model/restaurant.dart';
import 'package:restopective/widgets/card/customer_review.dart';
import 'package:restopective/widgets/card/restaurant_card.dart';

void main() {
  testWidgets("Show Restaurant Data", (WidgetTester tester) async {
    var resto = Restaurant(name: "Resto 1", city: "City", pictureId: "14");

    await mockNetworkImagesFor(() async => await tester.pumpWidget(MaterialApp(
          home: Scaffold(
            body: RestaurantCard(
              resto: resto,
            ),
          ),
        )));

    expect(find.text("Resto 1"), findsOneWidget);
    expect(find.text("City"), findsOneWidget);
  });

  testWidgets("Show Reviwer Data", (WidgetTester tester) async {
    var reviewer = CustomerReview(
        name: "Reviewer 1",
        date: "13-10-2021",
        review: "Makanannya enak banget");

    await mockNetworkImagesFor(() async => await tester.pumpWidget(MaterialApp(
          home: Scaffold(
            body: ReviewCard(
              customerReviews: reviewer,
            ),
          ),
        )));

    expect(find.text("Reviewer 1"), findsOneWidget);
    expect(find.text("13-10-2021"), findsOneWidget);
    expect(find.text("Makanannya enak banget"), findsOneWidget);
  });
}
