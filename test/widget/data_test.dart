import 'package:flutter_test/flutter_test.dart';
import 'package:restopective/data/model/customer_review.dart';
import 'package:restopective/data/model/food.dart';
import 'package:restopective/data/model/menus.dart';
import 'package:restopective/data/model/restaurant.dart';

void main() {
  test("Testing Parse", () {
    var data = {
      "id": "rqdv5juczeskfw1e867",
      "name": "Melting Pot",
      "description":
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ...",
      "city": "Medan",
      "address": "Jln. Pandeglang no 19",
      "pictureId": "14",
      "categories": [
        {"name": "Italia"},
        {"name": "Modern"}
      ],
      "menus": {
        "foods": [
          {"name": "Paket rosemary"},
          {"name": "Toastie salmon"}
        ],
        "drinks": [
          {"name": "Es krim"},
          {"name": "Sirup"}
        ]
      },
      "rating": 4.2,
      "customerReviews": [
        {
          "name": "Ahmad",
          "review": "Tidak rekomendasi untuk pelajar!",
          "date": "13 November 2019"
        }
      ]
    };

    var restaurant = Restaurant.fromJson(data);
    var restaurantMatch = Restaurant(
        id: "rqdv5juczeskfw1e867",
        name: "Melting Pot",
        description:
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ...",
        categories: const ["Italia", "Modern"],
        city: "Medan",
        pictureId: "14",
        menus: Menus(drinks: <Food>[
          Food(name: "Es krim"),
          Food(name: "Sirup")
        ], foods: <Food>[
          Food(name: "Paket rosemary"),
          Food(name: "Toastie salmon")
        ]),
        customerReviews: [
          CustomerReview(
              name: "Ahmad",
              review: "Tidak rekomendasi untuk pelajar!",
              date: "13 November 2019")
        ],
        rating: 4.2);

    expect(restaurant, restaurantMatch);
  });
}
